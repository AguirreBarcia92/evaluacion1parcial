package com.facci.aguirre.barcia.evaluacion1parciaaguirre;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class OperacionActivity extends AppCompatActivity {
    TextView texto;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operacion);

        texto =(TextView)findViewById(R.id.recibirtxt);
        Bundle bundle = this.getIntent().getExtras();

        double resultt;
        int libras;
        libras = Integer.parseInt(bundle.getString("dato"));
        resultt= libras*0.000453592;





        texto.setText(String.format("%.9f", resultt, "Tonaladas"));


        Log.e("operacion","coversion finalizada");
        MostrarMensaje("Coversion Finalizada");
    }

    private void MostrarMensaje(String mensaje) {
        Toast.makeText(
                this, mensaje, Toast.LENGTH_LONG
        ).show();
    }
}
